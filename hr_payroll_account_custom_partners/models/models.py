# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions

# class hr_payroll_account_custom_partners(models.Model):
#     _name = 'hr_payroll_account_custom_partners.hr_payroll_account_custom_partners'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100


class HrContributionRulePartner(models.Model):
    
    _name = 'hr_payroll_account_custom_partners.hr_contribution_rule_partner'
    _table = 'hr_contribution_rule_partner_rel'
    
    company_id = fields.Many2one('res.company', string='Company',
                                 default=lambda self: self.env['res.company']._company_default_get())
    name = fields.Char(string='Salary Rule Code', required=True)
    partner_id = fields.Many2one('res.partner', string='Partner', required=True)
    employee_ids = fields.Many2many('hr.employee', 'employee_rule_partner_rel', 'rule_id', 'emp_id', string='Employees')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Code already exists!')
    ]

    @api.constrains('name')
    def _check_name_in_code_salary_rule(self):
        for record in self:
            codes = self.env['hr.salary.rule'].search_count([['code', '=', record.name]])
            if codes == 0:
                raise exceptions.ValidationError("Name not match any *Code* in *Salary Rules*")
            
class Employee(models.Model):
    _inherit = 'hr.employee'

    rule_partner_ids = fields.Many2many('hr_payroll_account_custom_partners.hr_contribution_rule_partner', 'employee_rule_partner_rel', 'emp_id', 'rule_id', string='Rule Partners', readonly=True)

    account_partner_id = fields.Many2one('res.partner', 'Partner', required=True)


class HrPayslipLine(models.Model):
    _inherit = 'hr.payslip.line'

    def _get_partner_id(self, credit_account):
        """
        Get partner_id of slip line to use in account_move_line
        """
        matchs = self.env['hr_payroll_account_custom_partners.hr_contribution_rule_partner'].search(
            [['name', '=', self.salary_rule_id.code],
            ['employee_ids', 'in', [self.employee_id.id]]]
        )
        
        register_partner_id = self.salary_rule_id.register_id.partner_id
        partner_id = register_partner_id.id or self.employee_id.address_home_id.id

        if len(matchs) > 0:
            match = matchs[0]
            return match.partner_id.id

        if credit_account:
            if register_partner_id or self.salary_rule_id.account_credit.internal_type in ('receivable', 'payable'):
                return partner_id
        else:
            if register_partner_id or self.salary_rule_id.account_debit.internal_type in ('receivable', 'payable'):
                return partner_id

        if self.employee_id.account_partner_id:
            return self.employee_id.account_partner_id.id

        #TODO: es especifico de colombia
        #ya que todo movimiento lleva un tercero
        return partner_id


